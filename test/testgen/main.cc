#include <iostream>
#include <random>
#include <cmath>
#include <cstdint>

const auto NUMS = 100;

int8_t generateRandomNumber(unsigned int numBits) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dist(-128, 127);
    return dist(gen);
}

int modulo(int a, int b) {
  const int result = a % b;
  return result >= 0 ? result : result + b;
}

int main() {
    auto numsSoFar = 0;
    while (numsSoFar < NUMS) {
      int8_t num1 = generateRandomNumber(8);
      int8_t num2 = generateRandomNumber(8);
      int32_t sum = num1 - num2;

      if (sum < -128) {
          // Overflow occurred, adjust the sum
          int8_t over = sum;
          std::cout << "#(" << (int) num1 << ", " << (int) num2 << ", " << (int) over << ")," << std::endl;
          ++numsSoFar;
      }
    }

    return 0;
}
