import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}

const max_limit = 2015

const min_limit = 2007

pub opaque type Katzen {
  Katzen(data: BigInt)
}

pub fn to_bigint(value: Katzen) {
  value.data
}

pub fn limits() {
  interface.overflowable_limits(
    bigi.from_int(min_limit),
    bigi.from_int(max_limit),
  )
}

pub fn from_bigint_unsafe(value: BigInt) {
  Katzen(data: value)
}

pub const iface: Interface(Katzen, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)
