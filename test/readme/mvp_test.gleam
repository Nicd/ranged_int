import gleam/order
import bigi
import gleeunit/should
import ranged_int/interface
import ranged_int/utils
import readme/mvp

pub fn mvp_test() {
  let assert Ok(a) = interface.from_bigint(bigi.from_int(2007), mvp.iface)
  let assert Ok(b) = interface.from_bigint(bigi.from_int(2009), mvp.iface)
  let c = interface.compare(a, b, mvp.iface)
  should.equal(c, order.Lt)
}

pub fn mvp_add_test() {
  let assert Ok(a) = interface.from_bigint(bigi.from_int(2007), mvp.iface)
  let b = bigi.from_int(9)
  let c = interface.math_op(a, b, mvp.iface, bigi.add)
  should.equal(c, Error(utils.WouldOverflow(bigi.from_int(1))))
}
