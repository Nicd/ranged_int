import gleam/order
import bigi
import gleeunit/should
import ranged_int/utils
import readme/mvp2

pub fn mvp2_test() {
  let assert Ok(a) = mvp2.from_bigint(bigi.from_int(2007))
  let assert Ok(b) = mvp2.from_bigint(bigi.from_int(2009))
  let c = mvp2.compare(a, b)
  should.equal(c, order.Lt)
}

pub fn mvp2_add_test() {
  let assert Ok(a) = mvp2.from_bigint(bigi.from_int(2007))
  let b = bigi.from_int(9)
  let c = mvp2.add(a, b)
  should.equal(c, Error(utils.WouldOverflow(bigi.from_int(1))))
  should.equal(
    mvp2.overflow(c)
      |> mvp2.to_bigint(),
    bigi.from_int(2007),
  )
}
