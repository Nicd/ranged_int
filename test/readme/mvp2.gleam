import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}

const max_limit = 2015

const min_limit = 2007

pub opaque type Katzen {
  Katzen(data: BigInt)
}

pub fn to_bigint(value: Katzen) {
  value.data
}

fn limits() {
  interface.overflowable_limits(
    bigi.from_int(min_limit),
    bigi.from_int(max_limit),
  )
}

fn from_bigint_unsafe(value: BigInt) {
  Katzen(data: value)
}

const iface: Interface(Katzen, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn add(a: Katzen, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn compare(a: Katzen, b: Katzen) {
  interface.compare(a, b, iface)
}

pub fn overflow(res: interface.OpResult(Katzen)) {
  interface.overflow(res, iface)
}
