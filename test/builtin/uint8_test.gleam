import bigi
import gleeunit/should
import ranged_int/builtin/uint8
import ranged_int/builtin/uint

const min = 0

const max = 255

pub fn add_test() {
  let assert Ok(int1) = uint8.from_int(0)
  let assert int2 = bigi.from_int(1)
  should.equal(uint8.add(int1, int2), uint8.from_int(1))
}

pub fn subtract_test() {
  let assert Ok(int1) = uint8.from_int(1)
  let assert int2 = bigi.from_int(1)
  should.equal(uint8.subtract(int1, int2), uint8.from_int(0))
}

pub fn multiply_test() {
  let assert Ok(int1) = uint8.from_int(2)
  let assert int2 = bigi.from_int(2)
  should.equal(uint8.multiply(int1, int2), uint8.from_int(4))
}

pub fn divide_test() {
  let assert Ok(int1) = uint8.from_int(2)
  let assert int2 = bigi.from_int(2)
  should.equal(uint8.divide(int1, int2), uint8.from_int(1))
}

pub fn divide_no_zero_test() {
  let assert Ok(int1) = uint8.from_int(2)
  should.be_error(uint8.divide_no_zero(int1, bigi.zero()))
  should.equal(
    uint8.divide_no_zero(int1, bigi.from_int(2)),
    Ok(uint8.from_int(1)),
  )
}

pub fn modulo_test() {
  let assert Ok(int1) = uint8.from_int(2)
  let assert int2 = bigi.from_int(2)
  should.equal(uint8.modulo(int1, int2), uint8.from_int(0))
}

pub fn modulo_no_zero_test() {
  let assert Ok(int1) = uint8.from_int(2)
  should.be_error(uint8.modulo_no_zero(int1, bigi.zero()))
  should.equal(
    uint8.modulo_no_zero(int1, bigi.from_int(2)),
    Ok(uint8.from_int(0)),
  )
}

pub fn remainder_test() {
  let assert Ok(int1) = uint8.from_int(2)
  let assert int2 = bigi.from_int(2)
  should.equal(uint8.remainder(int1, int2), uint8.from_int(0))
}

pub fn remainder_no_zero_test() {
  let assert Ok(int1) = uint8.from_int(2)
  should.be_error(uint8.remainder_no_zero(int1, bigi.zero()))
  should.equal(
    uint8.remainder_no_zero(int1, bigi.from_int(2)),
    Ok(uint8.from_int(0)),
  )
}

pub fn power_test() {
  let assert Ok(int1) = uint8.from_int(2)
  let assert Ok(int2) = uint.from_bigint(bigi.from_int(2))
  should.equal(uint8.power(int1, int2), uint8.from_int(4))
}

pub fn overflow_test() {
  let assert Ok(int1) = uint8.from_int(max)
  let int2 = bigi.from_int(1)
  let assert Ok(expected) = uint8.from_int(min)
  should.equal(uint8.overflow(uint8.add(int1, int2)), expected)
}

pub fn eject_test() {
  let assert Ok(int1) = uint8.from_int(max)
  let int2 = bigi.from_int(1)
  should.equal(uint8.eject(uint8.add(int1, int2)), bigi.from_int(max + 1))
}
