import bigi
import gleeunit/should
import ranged_int/builtin/int128
import ranged_int/builtin/uint

pub fn absolute_test() {
  let assert Ok(int) = int128.from_bigint(bigi.from_int(-1))
  should.equal(int128.absolute(int), int128.from_bigint(bigi.from_int(1)))
}

pub fn add_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(-1))
  let assert int2 = bigi.from_int(1)
  should.equal(int128.add(int1, int2), int128.from_bigint(bigi.from_int(0)))
}

pub fn subtract_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(-1))
  let assert int2 = bigi.from_int(1)
  should.equal(
    int128.subtract(int1, int2),
    int128.from_bigint(bigi.from_int(-2)),
  )
}

pub fn multiply_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(2))
  let assert int2 = bigi.from_int(2)
  should.equal(
    int128.multiply(int1, int2),
    int128.from_bigint(bigi.from_int(4)),
  )
}

pub fn divide_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(-2))
  let assert int2 = bigi.from_int(-2)
  should.equal(int128.divide(int1, int2), int128.from_bigint(bigi.from_int(1)))
}

pub fn divide_no_zero_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(2))
  should.be_error(int128.divide_no_zero(int1, bigi.zero()))
  should.equal(
    int128.divide_no_zero(int1, bigi.from_int(2)),
    Ok(int128.from_bigint(bigi.from_int(1))),
  )
}

pub fn modulo_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(-2))
  let assert int2 = bigi.from_int(-2)
  should.equal(int128.modulo(int1, int2), int128.from_bigint(bigi.from_int(0)))
}

pub fn modulo_no_zero_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(2))
  should.be_error(int128.modulo_no_zero(int1, bigi.zero()))
  should.equal(
    int128.modulo_no_zero(int1, bigi.from_int(2)),
    Ok(int128.from_bigint(bigi.from_int(0))),
  )
}

pub fn remainder_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(-2))
  let assert int2 = bigi.from_int(-2)
  should.equal(
    int128.remainder(int1, int2),
    int128.from_bigint(bigi.from_int(0)),
  )
}

pub fn remainder_no_zero_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(2))
  should.be_error(int128.remainder_no_zero(int1, bigi.zero()))
  should.equal(
    int128.remainder_no_zero(int1, bigi.from_int(2)),
    Ok(int128.from_bigint(bigi.from_int(0))),
  )
}

pub fn power_test() {
  let assert Ok(int1) = int128.from_bigint(bigi.from_int(-2))
  let assert Ok(int2) = uint.from_bigint(bigi.from_int(2))
  should.equal(int128.power(int1, int2), int128.from_bigint(bigi.from_int(4)))
}

pub fn overflow_test() {
  let #(min, max) = minmax()
  let assert Ok(int1) = int128.from_bigint(max)
  let int2 = bigi.from_int(1)
  let assert Ok(expected) = int128.from_bigint(min)
  should.equal(int128.overflow(int128.add(int1, int2)), expected)
}

pub fn eject_test() {
  let #(_min, max) = minmax()
  let assert Ok(int1) = int128.from_bigint(max)
  let int2 = bigi.from_int(1)
  should.equal(
    int128.eject(int128.add(int1, int2)),
    bigi.add(max, bigi.from_int(1)),
  )
}

fn minmax() {
  let assert Ok(b127) = bigi.power(bigi.from_int(2), bigi.from_int(127))
  let min = bigi.multiply(b127, bigi.from_int(-1))
  let max = bigi.subtract(bigi.absolute(min), bigi.from_int(1))

  #(min, max)
}
