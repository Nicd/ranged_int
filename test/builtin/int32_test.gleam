import bigi
import gleeunit/should
import ranged_int/builtin/int32
import ranged_int/builtin/uint

const min = -2_147_483_648

const max = 2_147_483_647

pub fn absolute_test() {
  let assert Ok(int) = int32.from_int(-1)
  should.equal(int32.absolute(int), int32.from_int(1))
}

pub fn add_test() {
  let assert Ok(int1) = int32.from_int(-1)
  let assert int2 = bigi.from_int(1)
  should.equal(int32.add(int1, int2), int32.from_int(0))
}

pub fn subtract_test() {
  let assert Ok(int1) = int32.from_int(-1)
  let assert int2 = bigi.from_int(1)
  should.equal(int32.subtract(int1, int2), int32.from_int(-2))
}

pub fn multiply_test() {
  let assert Ok(int1) = int32.from_int(2)
  let assert int2 = bigi.from_int(2)
  should.equal(int32.multiply(int1, int2), int32.from_int(4))
}

pub fn divide_test() {
  let assert Ok(int1) = int32.from_int(-2)
  let assert int2 = bigi.from_int(-2)
  should.equal(int32.divide(int1, int2), int32.from_int(1))
}

pub fn divide_no_zero_test() {
  let assert Ok(int1) = int32.from_int(2)
  should.be_error(int32.divide_no_zero(int1, bigi.zero()))
  should.equal(
    int32.divide_no_zero(int1, bigi.from_int(2)),
    Ok(int32.from_int(1)),
  )
}

pub fn modulo_test() {
  let assert Ok(int1) = int32.from_int(-2)
  let assert int2 = bigi.from_int(-2)
  should.equal(int32.modulo(int1, int2), int32.from_int(0))
}

pub fn modulo_no_zero_test() {
  let assert Ok(int1) = int32.from_int(2)
  should.be_error(int32.modulo_no_zero(int1, bigi.zero()))
  should.equal(
    int32.modulo_no_zero(int1, bigi.from_int(2)),
    Ok(int32.from_int(0)),
  )
}

pub fn remainder_test() {
  let assert Ok(int1) = int32.from_int(-2)
  let assert int2 = bigi.from_int(-2)
  should.equal(int32.remainder(int1, int2), int32.from_int(0))
}

pub fn remainder_no_zero_test() {
  let assert Ok(int1) = int32.from_int(2)
  should.be_error(int32.remainder_no_zero(int1, bigi.zero()))
  should.equal(
    int32.remainder_no_zero(int1, bigi.from_int(2)),
    Ok(int32.from_int(0)),
  )
}

pub fn power_test() {
  let assert Ok(int1) = int32.from_int(-2)
  let assert Ok(int2) = uint.from_bigint(bigi.from_int(2))
  should.equal(int32.power(int1, int2), int32.from_int(4))
}

pub fn overflow_test() {
  let assert Ok(int1) = int32.from_int(max)
  let int2 = bigi.from_int(1)
  let assert Ok(expected) = int32.from_int(min)
  should.equal(int32.overflow(int32.add(int1, int2)), expected)
}

pub fn eject_test() {
  let assert Ok(int1) = int32.from_int(max)
  let int2 = bigi.from_int(1)
  should.equal(int32.eject(int32.add(int1, int2)), bigi.from_int(max + 1))
}
