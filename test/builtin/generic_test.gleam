import gleam/int
import gleam/float
import bigi
import gleeunit/should
import ranged_int/builtin/generic
import ranged_int/builtin/uint

const min = -31_495

const max = -13_413

pub fn absolute_test() {
  let int = from_int(min)
  should.equal(
    generic.to_bigint(generic.overflow(
      generic.absolute(int),
      generic.get_interface(int),
    )),
    bigi.from_int(-22_754),
  )
}

pub fn add_test() {
  let int1 = from_int(min)
  let int2 = bigi.from_int(1)
  let assert Ok(result) = generic.add(int1, int2)
  should.equal(generic.to_bigint(result), bigi.from_int(min + 1))
}

pub fn subtract_test() {
  let int1 = from_int(max)
  let int2 = bigi.from_int(1)
  let assert Ok(result) = generic.subtract(int1, int2)
  should.equal(generic.to_bigint(result), bigi.from_int(max - 1))
}

pub fn multiply_test() {
  let int1 = from_int(max)
  let int2 = bigi.from_int(2)
  let assert Ok(result) = generic.multiply(int1, int2)
  should.equal(generic.to_bigint(result), bigi.from_int(max * 2))
}

pub fn divide_test() {
  let int1 = from_int(min)
  let int2 = bigi.from_int(2)
  let assert Ok(result) = generic.divide(int1, int2)
  should.equal(generic.to_bigint(result), bigi.from_int(min / 2))
}

pub fn divide_no_zero_test() {
  let int1 = from_int(min)
  should.be_error(generic.divide_no_zero(int1, bigi.zero()))
  let assert Ok(Ok(result)) = generic.divide_no_zero(int1, bigi.from_int(2))
  should.equal(generic.to_bigint(result), bigi.from_int(min / 2))
}

pub fn modulo_test() {
  let int1 = from_int(min)
  let int2 = bigi.from_int(2)
  should.equal(
    generic.eject(generic.modulo(int1, int2), generic.get_interface(int1)),
    bigi.from_int(1),
  )
}

pub fn modulo_no_zero_test() {
  let int1 = from_int(min)
  should.be_error(generic.modulo_no_zero(int1, bigi.zero()))
  let assert Ok(rem) = generic.modulo_no_zero(int1, bigi.from_int(2))
  should.equal(
    generic.eject(rem, generic.get_interface(int1)),
    bigi.from_int(1),
  )
}

pub fn remainder_test() {
  let int1 = from_int(min)
  let int2 = bigi.from_int(2)
  should.equal(
    generic.eject(generic.remainder(int1, int2), generic.get_interface(int1)),
    bigi.from_int(-1),
  )
}

pub fn remainder_no_zero_test() {
  let int1 = from_int(min)
  should.be_error(generic.remainder_no_zero(int1, bigi.zero()))
  let assert Ok(rem) = generic.remainder_no_zero(int1, bigi.from_int(2))
  should.equal(
    generic.eject(rem, generic.get_interface(int1)),
    bigi.from_int(-1),
  )
}

pub fn power_test() {
  let int1 = from_int(max)
  let assert Ok(int2) = uint.from_bigint(bigi.from_int(2))
  let assert Ok(expected) = int.power(max, 2.0)
  should.equal(
    generic.eject(generic.power(int1, int2), generic.get_interface(int1)),
    bigi.from_int(float.truncate(expected)),
  )
}

fn from_int(int: Int) {
  let min = bigi.from_int(min)
  let max = bigi.from_int(max)

  let assert Ok(int) =
    int
    |> bigi.from_int()
    |> generic.from_bigint_overflowable(min: min, max: max)

  int
}
