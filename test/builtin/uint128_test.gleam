import bigi
import gleeunit/should
import ranged_int/builtin/uint128
import ranged_int/builtin/uint

pub fn add_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(0))
  let assert int2 = bigi.from_int(1)
  should.equal(uint128.add(int1, int2), uint128.from_bigint(bigi.from_int(1)))
}

pub fn subtract_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(1))
  let assert int2 = bigi.from_int(1)
  should.equal(
    uint128.subtract(int1, int2),
    uint128.from_bigint(bigi.from_int(0)),
  )
}

pub fn multiply_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  let assert int2 = bigi.from_int(2)
  should.equal(
    uint128.multiply(int1, int2),
    uint128.from_bigint(bigi.from_int(4)),
  )
}

pub fn divide_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  let assert int2 = bigi.from_int(2)
  should.equal(
    uint128.divide(int1, int2),
    uint128.from_bigint(bigi.from_int(1)),
  )
}

pub fn divide_no_zero_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  should.be_error(uint128.divide_no_zero(int1, bigi.zero()))
  should.equal(
    uint128.divide_no_zero(int1, bigi.from_int(2)),
    Ok(uint128.from_bigint(bigi.from_int(1))),
  )
}

pub fn modulo_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  let assert int2 = bigi.from_int(2)
  should.equal(
    uint128.modulo(int1, int2),
    uint128.from_bigint(bigi.from_int(0)),
  )
}

pub fn modulo_no_zero_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  should.be_error(uint128.modulo_no_zero(int1, bigi.zero()))
  should.equal(
    uint128.modulo_no_zero(int1, bigi.from_int(2)),
    Ok(uint128.from_bigint(bigi.from_int(0))),
  )
}

pub fn remainder_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  let assert int2 = bigi.from_int(2)
  should.equal(
    uint128.remainder(int1, int2),
    uint128.from_bigint(bigi.from_int(0)),
  )
}

pub fn remainder_no_zero_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  should.be_error(uint128.remainder_no_zero(int1, bigi.zero()))
  should.equal(
    uint128.remainder_no_zero(int1, bigi.from_int(2)),
    Ok(uint128.from_bigint(bigi.from_int(0))),
  )
}

pub fn power_test() {
  let assert Ok(int1) = uint128.from_bigint(bigi.from_int(2))
  let assert Ok(int2) = uint.from_bigint(bigi.from_int(2))
  should.equal(uint128.power(int1, int2), uint128.from_bigint(bigi.from_int(4)))
}

pub fn overflow_test() {
  let #(min, max) = minmax()
  let assert Ok(int1) = uint128.from_bigint(max)
  let int2 = bigi.from_int(1)
  let assert Ok(expected) = uint128.from_bigint(min)
  should.equal(uint128.overflow(uint128.add(int1, int2)), expected)
}

pub fn eject_test() {
  let #(_min, max) = minmax()
  let assert Ok(int1) = uint128.from_bigint(max)
  let int2 = bigi.from_int(1)
  should.equal(
    uint128.eject(uint128.add(int1, int2)),
    bigi.add(max, bigi.from_int(1)),
  )
}

fn minmax() {
  let assert Ok(b128) = bigi.power(bigi.from_int(2), bigi.from_int(128))
  let min = bigi.zero()
  let max = bigi.subtract(b128, bigi.from_int(1))

  #(min, max)
}
