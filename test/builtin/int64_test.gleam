import bigi
import gleeunit/should
import ranged_int/builtin/int64
import ranged_int/builtin/uint

pub fn absolute_test() {
  let assert Ok(int) = int64.from_bigint(bigi.from_int(-1))
  should.equal(int64.absolute(int), int64.from_bigint(bigi.from_int(1)))
}

pub fn add_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(-1))
  let assert int2 = bigi.from_int(1)
  should.equal(int64.add(int1, int2), int64.from_bigint(bigi.from_int(0)))
}

pub fn subtract_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(-1))
  let assert int2 = bigi.from_int(1)
  should.equal(int64.subtract(int1, int2), int64.from_bigint(bigi.from_int(-2)))
}

pub fn multiply_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(2))
  let assert int2 = bigi.from_int(2)
  should.equal(int64.multiply(int1, int2), int64.from_bigint(bigi.from_int(4)))
}

pub fn divide_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(-2))
  let assert int2 = bigi.from_int(-2)
  should.equal(int64.divide(int1, int2), int64.from_bigint(bigi.from_int(1)))
}

pub fn divide_no_zero_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(2))
  should.be_error(int64.divide_no_zero(int1, bigi.zero()))
  should.equal(
    int64.divide_no_zero(int1, bigi.from_int(2)),
    Ok(int64.from_bigint(bigi.from_int(1))),
  )
}

pub fn modulo_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(-2))
  let assert int2 = bigi.from_int(-2)
  should.equal(int64.modulo(int1, int2), int64.from_bigint(bigi.from_int(0)))
}

pub fn modulo_no_zero_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(2))
  should.be_error(int64.modulo_no_zero(int1, bigi.zero()))
  should.equal(
    int64.modulo_no_zero(int1, bigi.from_int(2)),
    Ok(int64.from_bigint(bigi.from_int(0))),
  )
}

pub fn remainder_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(-2))
  let assert int2 = bigi.from_int(-2)
  should.equal(int64.remainder(int1, int2), int64.from_bigint(bigi.from_int(0)))
}

pub fn remainder_no_zero_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(2))
  should.be_error(int64.remainder_no_zero(int1, bigi.zero()))
  should.equal(
    int64.remainder_no_zero(int1, bigi.from_int(2)),
    Ok(int64.from_bigint(bigi.from_int(0))),
  )
}

pub fn power_test() {
  let assert Ok(int1) = int64.from_bigint(bigi.from_int(-2))
  let assert Ok(int2) = uint.from_bigint(bigi.from_int(2))
  should.equal(int64.power(int1, int2), int64.from_bigint(bigi.from_int(4)))
}

pub fn overflow_test() {
  let #(min, max) = minmax()
  let assert Ok(int1) = int64.from_bigint(max)
  let int2 = bigi.from_int(1)
  let assert Ok(expected) = int64.from_bigint(min)
  should.equal(int64.overflow(int64.add(int1, int2)), expected)
}

pub fn eject_test() {
  let #(_min, max) = minmax()
  let assert Ok(int1) = int64.from_bigint(max)
  let int2 = bigi.from_int(1)
  should.equal(
    int64.eject(int64.add(int1, int2)),
    bigi.add(max, bigi.from_int(1)),
  )
}

fn minmax() {
  let assert Ok(b63) = bigi.power(bigi.from_int(2), bigi.from_int(63))
  let min = bigi.multiply(b63, bigi.from_int(-1))
  let max = bigi.subtract(bigi.absolute(min), bigi.from_int(1))

  #(min, max)
}
