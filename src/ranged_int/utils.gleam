import bigi.{type BigInt}

/// An overflow would have occurred, i.e. the result did not fit the allowed
/// range.
pub type Overflow {
  /// The result was this much higher than the maximum allowed value.
  WouldOverflow(BigInt)
  /// The result was this much lower than the minimum allowed value.
  WouldUnderflow(BigInt)
}

/// Calculate the overflowed value given an overflow result and the limits.
pub fn overflow(overflow: Overflow, min min: BigInt, max max: BigInt) -> BigInt {
  let total_values =
    bigi.subtract(max, min)
    |> bigi.add(bigi.from_int(1))

  let actual = case overflow {
    WouldOverflow(amount) -> bigi.add(max, amount)
    WouldUnderflow(amount) -> bigi.subtract(min, amount)
  }

  actual
  |> bigi.subtract(min)
  |> bigi.modulo(total_values)
  |> bigi.add(min)
}
