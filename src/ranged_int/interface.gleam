//// The ranged integer "interface".
////
//// This module contains types and functions to be implemented and used to
//// create custom ranged integer types.
////
//// See the README for instructions on how to create your own type with this
//// module.

import gleam/order
import gleam/result
import bigi.{type BigInt}
import ranged_int/internal/limit.{type Limit, NoOverflow, Overflow, Underflow}
import ranged_int/utils

/// Mark this ranged integer as overflowable. An overflowable integer must have
/// both a minimum and maximum limit.
pub type Overflowable {
  Overflowable
}

/// Mark this ranged integer as non-overflowable. A non-overflowable integer
/// can only have a minimum or a maximum limit, but cannot use the `overflow`
/// function.
pub type NonOverflowable {
  NonOverflowable
}

/// The minimum and maximum limits of a ranged integer.
pub opaque type Limits(overflow_mode) {
  Limits(min: Limit, max: Limit)
}

// Helper type for limits when we know that the integer is overflowable.
type OverflowableLimits {
  OverflowableLimits(min: BigInt, max: BigInt)
}

/// Result of math operations: a new ranged integer or an overflow.
///
/// An overflow result may occur even for non-overflowable integers. The
/// difference is that the `overflow` function cannot be called to calculate the
/// overflowed result for a non-overflowable integer.
pub type OpResult(a) =
  Result(a, utils.Overflow)

/// The ranged integer interface that needs to be passed to math operations.
///
/// To allow for overflowing, the second type parameter must be `Overflowable`.
pub type Interface(a, overflow_mode) {
  Interface(
    /// A function to convert the ranged integer into a big integer.
    to_bigint: fn(a) -> BigInt,
    /// A function to convert a big integer to the ranged integer. No limit
    /// checking is done, hence the name "unsafe". This is used internally when
    /// the limits have already been checked.
    from_bigint_unsafe: fn(BigInt) -> a,
    /// A function to return the limits of the ranged integer.
    limits: fn() -> Limits(overflow_mode),
  )
}

/// Construct limits for an overflowable ranged integer. The given limits are
/// inclusive.
pub fn overflowable_limits(min: BigInt, max: BigInt) -> Limits(Overflowable) {
  Limits(min: limit.Bounded(min), max: limit.Bounded(max))
}

/// Construct limits for a non-overflowable ranged integer that only has a
/// maximum limit. The given limit is inclusive.
pub fn max_limit(max: BigInt) -> Limits(NonOverflowable) {
  Limits(min: limit.Unbounded, max: limit.Bounded(max))
}

/// Construct limits for a non-overflowable ranged integer that only has a
/// minimum limit. The given limit is inclusive.
pub fn min_limit(min: BigInt) -> Limits(NonOverflowable) {
  Limits(min: limit.Bounded(min), max: limit.Unbounded)
}

/// Create a ranged integer from a big integer.
pub fn from_bigint(
  value: BigInt,
  interface: Interface(a, overflow_mode),
) -> OpResult(a) {
  let limits = interface.limits()
  case limit.check_limits(value, min: limits.min, max: limits.max) {
    NoOverflow -> Ok(interface.from_bigint_unsafe(value))
    Overflow(amount) -> Error(utils.WouldOverflow(amount))
    Underflow(amount) -> Error(utils.WouldUnderflow(amount))
  }
}

/// Compare two ranged integers, returning an order that denotes if `int1` is
/// lower, bigger than, or equal to `int2`.
pub fn compare(
  int1: a,
  int2: a,
  interface: Interface(a, overflow_mode),
) -> order.Order {
  bigi.compare(interface.to_bigint(int1), interface.to_bigint(int2))
}

/// Run a math operation on a ranged integer and an unranged big integer.
pub fn math_op(
  int1: a,
  int2: BigInt,
  interface: Interface(a, overflow_mode),
  op: fn(BigInt, BigInt) -> BigInt,
) -> OpResult(a) {
  let limits = interface.limits()
  let int1 = interface.to_bigint(int1)
  let result = op(int1, int2)
  case limit.check_limits(result, min: limits.min, max: limits.max) {
    NoOverflow -> Ok(interface.from_bigint_unsafe(result))
    Overflow(amount) -> Error(utils.WouldOverflow(amount))
    Underflow(amount) -> Error(utils.WouldUnderflow(amount))
  }
}

/// Run a unary math operation on a ranged integer.
pub fn math_op_unary(
  int: a,
  interface: Interface(a, overflow_mode),
  op: fn(BigInt) -> BigInt,
) -> OpResult(a) {
  let limits = interface.limits()
  let int = interface.to_bigint(int)
  let result = op(int)
  case limit.check_limits(result, min: limits.min, max: limits.max) {
    NoOverflow -> Ok(interface.from_bigint_unsafe(result))
    Overflow(amount) -> Error(utils.WouldOverflow(amount))
    Underflow(amount) -> Error(utils.WouldUnderflow(amount))
  }
}

/// Run a math operation that may fail on a ranged integer and an unranged big
/// integer.
///
/// Some functions such as `bigi.divide_no_zero` will return an error if the
/// arguments are invalid. This error is passed upwards by this function.
pub fn fallible_op(
  int1: a,
  int2: BigInt,
  interface: Interface(a, overflow_mode),
  op: fn(BigInt, BigInt) -> Result(BigInt, op_err),
) -> Result(OpResult(a), op_err) {
  let limits = interface.limits()
  let int1 = interface.to_bigint(int1)
  use result <- result.try(op(int1, int2))
  Ok(case limit.check_limits(result, min: limits.min, max: limits.max) {
    NoOverflow -> Ok(interface.from_bigint_unsafe(result))
    Overflow(amount) -> Error(utils.WouldOverflow(amount))
    Underflow(amount) -> Error(utils.WouldUnderflow(amount))
  })
}

/// Map the overflow result of an operation to the integer's allowed range.
///
/// The overflow is calculated using a modulo against the amount of integers in
/// the allowed range. This matches the unsigned integer overflow logic of C/C++
/// but is also defined for signed (negative) values.
///
/// This can only be called for overflowable integers.
pub fn overflow(value: OpResult(a), interface: Interface(a, Overflowable)) {
  case value {
    Ok(new_value) -> new_value
    Error(overflow) -> {
      let OverflowableLimits(min, max) = destructure_limits(interface.limits())
      utils.overflow(overflow, min: min, max: max)
      |> interface.from_bigint_unsafe()
    }
  }
}

/// Eject the result of an operation to big integer. The big integer may be
/// outside the range of the original ranged integers.
pub fn eject(value: OpResult(a), interface: Interface(a, overflow_mode)) {
  case value {
    Ok(new_value) -> interface.to_bigint(new_value)
    Error(overflow) -> {
      case overflow, interface.limits() {
        utils.WouldOverflow(amount), Limits(max: limit.Bounded(max), ..) ->
          bigi.add(max, amount)
        utils.WouldUnderflow(amount), Limits(min: limit.Bounded(min), ..) ->
          bigi.subtract(min, amount)
        _, _ -> panic as "Overflowed but there was no corresponding limit (o_O)"
      }
    }
  }
}

fn destructure_limits(limits: Limits(Overflowable)) {
  // We can trust that overflowable limits are always bounded on both sides
  let assert limit.Bounded(min) = limits.min
  let assert limit.Bounded(max) = limits.max
  OverflowableLimits(min: min, max: max)
}
