import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}
import ranged_int/builtin/uint

pub opaque type Uint128 {
  Uint128(data: BigInt)
}

const iface: Interface(Uint128, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn to_bigint(uint: Uint128) {
  uint.data
}

pub fn compare(a: Uint128, b: Uint128) {
  interface.compare(a, b, iface)
}

pub fn add(a: Uint128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn subtract(a: Uint128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.subtract)
}

pub fn multiply(a: Uint128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.multiply)
}

pub fn divide(a: Uint128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.divide)
}

pub fn divide_no_zero(a: Uint128, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.divide_no_zero)
}

pub fn modulo(a: Uint128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.modulo)
}

pub fn modulo_no_zero(a: Uint128, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.modulo_no_zero)
}

pub fn remainder(a: Uint128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.remainder)
}

pub fn remainder_no_zero(a: Uint128, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.remainder_no_zero)
}

pub fn power(a: Uint128, b: uint.Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, uint.to_bigint(b), iface, bigi.power)
  result
}

pub fn overflow(op: interface.OpResult(Uint128)) {
  interface.overflow(op, iface)
}

pub fn eject(op: interface.OpResult(Uint128)) {
  interface.eject(op, iface)
}

fn limits() {
  let min = bigi.zero()
  let assert Ok(b128) = bigi.power(bigi.from_int(2), bigi.from_int(128))
  let max = bigi.subtract(b128, bigi.from_int(1))

  interface.overflowable_limits(min, max)
}

fn from_bigint_unsafe(value: BigInt) {
  Uint128(data: value)
}
