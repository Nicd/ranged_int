import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}
import ranged_int/builtin/uint

pub opaque type Int64 {
  Int64(data: BigInt)
}

const iface: Interface(Int64, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn to_bigint(uint: Int64) {
  uint.data
}

pub fn compare(a: Int64, b: Int64) {
  interface.compare(a, b, iface)
}

pub fn absolute(a: Int64) {
  interface.math_op_unary(a, iface, bigi.absolute)
}

pub fn add(a: Int64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn subtract(a: Int64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.subtract)
}

pub fn multiply(a: Int64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.multiply)
}

pub fn divide(a: Int64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.divide)
}

pub fn divide_no_zero(a: Int64, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.divide_no_zero)
}

pub fn modulo(a: Int64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.modulo)
}

pub fn modulo_no_zero(a: Int64, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.modulo_no_zero)
}

pub fn remainder(a: Int64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.remainder)
}

pub fn remainder_no_zero(a: Int64, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.remainder_no_zero)
}

pub fn power(a: Int64, b: uint.Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, uint.to_bigint(b), iface, bigi.power)
  result
}

pub fn overflow(op: interface.OpResult(Int64)) {
  interface.overflow(op, iface)
}

pub fn eject(op: interface.OpResult(Int64)) {
  interface.eject(op, iface)
}

fn limits() {
  let assert Ok(b63) = bigi.power(bigi.from_int(2), bigi.from_int(63))
  let min = bigi.multiply(b63, bigi.from_int(-1))
  let max = bigi.subtract(bigi.absolute(min), bigi.from_int(1))

  interface.overflowable_limits(min, max)
}

fn from_bigint_unsafe(value: BigInt) {
  Int64(data: value)
}
