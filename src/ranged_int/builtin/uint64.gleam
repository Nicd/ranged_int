import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}
import ranged_int/builtin/uint

pub opaque type Uint64 {
  Uint64(data: BigInt)
}

const iface: Interface(Uint64, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn to_bigint(uint: Uint64) {
  uint.data
}

pub fn compare(a: Uint64, b: Uint64) {
  interface.compare(a, b, iface)
}

pub fn add(a: Uint64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn subtract(a: Uint64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.subtract)
}

pub fn multiply(a: Uint64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.multiply)
}

pub fn divide(a: Uint64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.divide)
}

pub fn divide_no_zero(a: Uint64, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.divide_no_zero)
}

pub fn modulo(a: Uint64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.modulo)
}

pub fn modulo_no_zero(a: Uint64, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.modulo_no_zero)
}

pub fn remainder(a: Uint64, b: BigInt) {
  interface.math_op(a, b, iface, bigi.remainder)
}

pub fn remainder_no_zero(a: Uint64, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.remainder_no_zero)
}

pub fn power(a: Uint64, b: uint.Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, uint.to_bigint(b), iface, bigi.power)
  result
}

pub fn overflow(op: interface.OpResult(Uint64)) {
  interface.overflow(op, iface)
}

pub fn eject(op: interface.OpResult(Uint64)) {
  interface.eject(op, iface)
}

fn limits() {
  let min = bigi.zero()
  let assert Ok(b64) = bigi.power(bigi.from_int(2), bigi.from_int(64))
  let max = bigi.subtract(b64, bigi.from_int(1))

  interface.overflowable_limits(min, max)
}

fn from_bigint_unsafe(value: BigInt) {
  Uint64(data: value)
}
