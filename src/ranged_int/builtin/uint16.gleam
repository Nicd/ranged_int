import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}
import ranged_int/builtin/uint

const max_limit = 65_535

const min_limit = 0

pub opaque type Uint16 {
  Uint16(data: BigInt)
}

const iface: Interface(Uint16, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn from_int(value: Int) {
  from_bigint(bigi.from_int(value))
}

pub fn to_int(uint: Uint16) {
  let assert Ok(int) =
    uint
    |> to_bigint()
    |> bigi.to_int()
  int
}

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn to_bigint(uint: Uint16) {
  uint.data
}

pub fn compare(a: Uint16, b: Uint16) {
  interface.compare(a, b, iface)
}

pub fn add(a: Uint16, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn subtract(a: Uint16, b: BigInt) {
  interface.math_op(a, b, iface, bigi.subtract)
}

pub fn multiply(a: Uint16, b: BigInt) {
  interface.math_op(a, b, iface, bigi.multiply)
}

pub fn divide(a: Uint16, b: BigInt) {
  interface.math_op(a, b, iface, bigi.divide)
}

pub fn divide_no_zero(a: Uint16, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.divide_no_zero)
}

pub fn modulo(a: Uint16, b: BigInt) {
  interface.math_op(a, b, iface, bigi.modulo)
}

pub fn modulo_no_zero(a: Uint16, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.modulo_no_zero)
}

pub fn remainder(a: Uint16, b: BigInt) {
  interface.math_op(a, b, iface, bigi.remainder)
}

pub fn remainder_no_zero(a: Uint16, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.remainder_no_zero)
}

pub fn power(a: Uint16, b: uint.Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, uint.to_bigint(b), iface, bigi.power)
  result
}

pub fn overflow(op: interface.OpResult(Uint16)) {
  interface.overflow(op, iface)
}

pub fn eject(op: interface.OpResult(Uint16)) {
  interface.eject(op, iface)
}

fn limits() {
  interface.overflowable_limits(
    bigi.from_int(min_limit),
    bigi.from_int(max_limit),
  )
}

fn from_bigint_unsafe(value: BigInt) {
  Uint16(data: value)
}
