//// A generic ranged integer.
////
//// This module is meant for cases where the integer range cannot be known at
//// compile time. Generic ranged integers are less type safe and have lower
//// performance. It's always suggested to use one of the builtin types or to
//// create your own type when you can.

import bigi.{type BigInt}
import ranged_int/interface.{type Interface, type Overflowable, Interface}
import ranged_int/builtin/uint

/// The interface of a generic ranged integer.
pub opaque type GenericInterface(overflow_mode) {
  GenericInterface(
    interface: Interface(RangedInt(overflow_mode), overflow_mode),
  )
}

/// A generic ranged integer, carrying its own interface along with the data.
pub opaque type RangedInt(overflow_mode) {
  RangedInt(
    data: BigInt,
    interface: fn() -> Interface(RangedInt(overflow_mode), overflow_mode),
  )
}

/// Create from a big integer and a minimum and maximum value, both inclusive.
/// The created integer can be used with `overflow`.
pub fn from_bigint_overflowable(value: BigInt, min min: BigInt, max max: BigInt) {
  let iface = gen_overflowable_interface(min, max)
  interface.from_bigint(value, iface)
}

/// Create from a big integer and a minimum value, inclusive.
pub fn from_bigint_min(value: BigInt, min min: BigInt) {
  let iface = gen_min_interface(min)
  interface.from_bigint(value, iface)
}

/// Create from a big integer and a maximum value, inclusive.
pub fn from_bigint_max(value: BigInt, max max: BigInt) {
  let iface = gen_max_interface(max)
  interface.from_bigint(value, iface)
}

pub fn to_bigint(int: RangedInt(overflow_mode)) {
  int.data
}

/// Get the interface of the integer. This interface is required for `overflow`
/// and `eject`.
pub fn get_interface(int: RangedInt(overflow_mode)) {
  GenericInterface(int.interface())
}

pub fn compare(a: RangedInt(overflow_mode), b: RangedInt(overflow_mode)) {
  interface.compare(a, b, a.interface())
}

pub fn absolute(a: RangedInt(overflow_mode)) {
  interface.math_op_unary(a, a.interface(), bigi.absolute)
}

pub fn add(a: RangedInt(overflow_mode), b: BigInt) {
  interface.math_op(a, b, a.interface(), bigi.add)
}

pub fn subtract(a: RangedInt(overflow_mode), b: BigInt) {
  interface.math_op(a, b, a.interface(), bigi.subtract)
}

pub fn multiply(a: RangedInt(overflow_mode), b: BigInt) {
  interface.math_op(a, b, a.interface(), bigi.multiply)
}

pub fn divide(a: RangedInt(overflow_mode), b: BigInt) {
  interface.math_op(a, b, a.interface(), bigi.divide)
}

pub fn divide_no_zero(a: RangedInt(overflow_mode), b: BigInt) {
  interface.fallible_op(a, b, a.interface(), bigi.divide_no_zero)
}

pub fn modulo(a: RangedInt(overflow_mode), b: BigInt) {
  interface.math_op(a, b, a.interface(), bigi.modulo)
}

pub fn modulo_no_zero(a: RangedInt(overflow_mode), b: BigInt) {
  interface.fallible_op(a, b, a.interface(), bigi.modulo_no_zero)
}

pub fn remainder(a: RangedInt(overflow_mode), b: BigInt) {
  interface.math_op(a, b, a.interface(), bigi.remainder)
}

pub fn remainder_no_zero(a: RangedInt(overflow_mode), b: BigInt) {
  interface.fallible_op(a, b, a.interface(), bigi.remainder_no_zero)
}

pub fn power(a: RangedInt(overflow_mode), b: uint.Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, uint.to_bigint(b), a.interface(), bigi.power)
  result
}

pub fn overflow(
  op: interface.OpResult(RangedInt(Overflowable)),
  interface: GenericInterface(Overflowable),
) {
  interface.overflow(op, interface.interface)
}

pub fn eject(
  op: interface.OpResult(RangedInt(overflow_mode)),
  interface: GenericInterface(overflow_mode),
) {
  interface.eject(op, interface.interface)
}

fn gen_overflowable_interface(min: BigInt, max: BigInt) {
  Interface(
    from_bigint_unsafe: fn(b: BigInt) {
      RangedInt(data: b, interface: fn() {
        gen_overflowable_interface(min, max)
      })
    },
    limits: fn() { interface.overflowable_limits(min, max) },
    to_bigint: to_bigint,
  )
}

fn gen_min_interface(min: BigInt) {
  Interface(
    from_bigint_unsafe: fn(b: BigInt) {
      RangedInt(data: b, interface: fn() { gen_min_interface(min) })
    },
    limits: fn() { interface.min_limit(min) },
    to_bigint: to_bigint,
  )
}

fn gen_max_interface(max: BigInt) {
  Interface(
    from_bigint_unsafe: fn(b: BigInt) {
      RangedInt(data: b, interface: fn() { gen_max_interface(max) })
    },
    limits: fn() { interface.max_limit(max) },
    to_bigint: to_bigint,
  )
}
