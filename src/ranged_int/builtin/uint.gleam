//// An unsigned integer ranging from 0 and upwards.

import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}

const min_limit = 0

pub opaque type Uint {
  Uint(data: BigInt)
}

const iface: Interface(Uint, interface.NonOverflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn to_bigint(uint: Uint) {
  uint.data
}

pub fn compare(a: Uint, b: Uint) {
  interface.compare(a, b, iface)
}

pub fn add(a: Uint, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn subtract(a: Uint, b: BigInt) {
  interface.math_op(a, b, iface, bigi.subtract)
}

pub fn multiply(a: Uint, b: BigInt) {
  interface.math_op(a, b, iface, bigi.multiply)
}

pub fn divide(a: Uint, b: BigInt) {
  interface.math_op(a, b, iface, bigi.divide)
}

pub fn modulo(a: Uint, b: BigInt) {
  interface.math_op(a, b, iface, bigi.modulo)
}

pub fn remainder(a: Uint, b: BigInt) {
  interface.math_op(a, b, iface, bigi.remainder)
}

pub fn power(a: Uint, b: Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, to_bigint(b), iface, bigi.power)
  result
}

pub fn eject(op: interface.OpResult(Uint)) {
  interface.eject(op, iface)
}

fn limits() {
  interface.min_limit(bigi.from_int(min_limit))
}

fn from_bigint_unsafe(value: BigInt) {
  Uint(data: value)
}
