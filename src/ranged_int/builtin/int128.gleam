import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}
import ranged_int/builtin/uint

pub opaque type Int128 {
  Int128(data: BigInt)
}

const iface: Interface(Int128, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub fn from_bigint(value: BigInt) {
  interface.from_bigint(value, iface)
}

pub fn to_bigint(uint: Int128) {
  uint.data
}

pub fn compare(a: Int128, b: Int128) {
  interface.compare(a, b, iface)
}

pub fn absolute(a: Int128) {
  interface.math_op_unary(a, iface, bigi.absolute)
}

pub fn add(a: Int128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.add)
}

pub fn subtract(a: Int128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.subtract)
}

pub fn multiply(a: Int128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.multiply)
}

pub fn divide(a: Int128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.divide)
}

pub fn divide_no_zero(a: Int128, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.divide_no_zero)
}

pub fn modulo(a: Int128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.modulo)
}

pub fn modulo_no_zero(a: Int128, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.modulo_no_zero)
}

pub fn remainder(a: Int128, b: BigInt) {
  interface.math_op(a, b, iface, bigi.remainder)
}

pub fn remainder_no_zero(a: Int128, b: BigInt) {
  interface.fallible_op(a, b, iface, bigi.remainder_no_zero)
}

pub fn power(a: Int128, b: uint.Uint) {
  let assert Ok(result) =
    interface.fallible_op(a, uint.to_bigint(b), iface, bigi.power)
  result
}

pub fn overflow(op: interface.OpResult(Int128)) {
  interface.overflow(op, iface)
}

pub fn eject(op: interface.OpResult(Int128)) {
  interface.eject(op, iface)
}

fn limits() {
  let assert Ok(b127) = bigi.power(bigi.from_int(2), bigi.from_int(127))
  let min = bigi.multiply(b127, bigi.from_int(-1))
  let max = bigi.subtract(bigi.absolute(min), bigi.from_int(1))

  interface.overflowable_limits(min, max)
}

fn from_bigint_unsafe(value: BigInt) {
  Int128(data: value)
}
