import gleam/order
import bigi.{type BigInt}

/// A limit for an integer. Each direction (max, min) may have its own limit.
pub type Limit {
  /// The integer is limited to this value inclusive.
  Bounded(BigInt)

  /// The integer is unbounded.
  Unbounded
}

/// Result of a limit check.
pub type LimitCheck {
  /// No overflow was detected.
  NoOverflow

  /// The amount would overflow the allowed range by this amount.
  Overflow(BigInt)

  /// The amount would underflow (be lower than the minimum) by this amount.
  Underflow(BigInt)
}

/// Check the value against the limits.
pub fn check_limits(value: BigInt, min min: Limit, max max: Limit) -> LimitCheck {
  let max_diff = diff(max, value)
  case bigi.compare(max_diff, bigi.zero()) {
    order.Gt -> Overflow(max_diff)
    order.Eq | order.Lt -> {
      let min_diff = diff(min, value)
      case bigi.compare(min_diff, bigi.zero()) {
        order.Lt -> Underflow(bigi.multiply(min_diff, bigi.from_int(-1)))
        order.Eq | order.Gt -> NoOverflow
      }
    }
  }
}

fn diff(limit: Limit, value: BigInt) {
  case limit {
    Unbounded -> bigi.zero()
    Bounded(lim) -> bigi.subtract(value, lim)
  }
}
